package trello.com.trellocards;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import butterknife.ButterKnife;
import butterknife.InjectView;
import trello.com.trellocards.utils.Debug;

public class StartActivity extends BaseActivity {
    @InjectView(R.id.webview)
    WebView webView;

    private String currentUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.inject(this);

        setWebView();
        webView.loadUrl(TrelloConstants.TOKEN_REQUEST_URL);
    }

    public void onGetToken(String token) {
        Debug.debug(token);
        saveToken(token);
        startMainActivity();
    }

    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void setWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                currentUrl = url;
                webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
                webView.loadUrl("javascript:window.HTMLOUT.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }

        });
    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        public void showHTML(final String html) {
            if(currentUrl!=null && currentUrl.equals("https://trello.com/1/token/approve")) {
                Document document = Jsoup.parse(html);
                Element element = document.select("pre").first();
                if (element != null) {
                    String token = element.text();
                    token = token.replaceAll("\\s", "");
                    onGetToken(token);
                }
            }
        }
    }

}

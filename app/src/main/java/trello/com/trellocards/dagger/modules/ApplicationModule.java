package trello.com.trellocards.dagger.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import trello.com.trellocards.BuildConfig;
import trello.com.trellocards.TrelloApplication;
import trello.com.trellocards.api.ApiClient;
import trello.com.trellocards.converter.JacksonConverter;
import trello.com.trellocards.dagger.annotations.ForApplication;
import trello.com.trellocards.utils.Debug;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */
@Module(library = true, injects = {TrelloApplication.class})
public class ApplicationModule {
    private final TrelloApplication application;

    public ApplicationModule(TrelloApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    Bus provideBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    ApiClient provideApiClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(BuildConfig.API_URL);
        builder.setConverter(new JacksonConverter());
        builder.setLog(new RestAdapter.Log() {
            @Override
            public void log(String message) {
                Debug.debug(message);
            }
        });
        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setClient(new OkClient());
        RestAdapter adapter = builder.build();

        return adapter.create(ApiClient.class);
    }

    @Provides
    @Singleton
    ObjectMapper provideObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        return mapper;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return application.getSharedPreferences(BuildConfig.APPLICATION_NAME, Context.MODE_PRIVATE);
    }


}

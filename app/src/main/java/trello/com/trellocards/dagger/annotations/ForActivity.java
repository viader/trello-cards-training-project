package trello.com.trellocards.dagger.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */

@Qualifier
@Retention(RUNTIME)
public @interface ForActivity {
}

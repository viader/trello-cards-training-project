package trello.com.trellocards.dagger.modules;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import trello.com.trellocards.BaseActivity;
import trello.com.trellocards.MainActivity;
import trello.com.trellocards.StartActivity;
import trello.com.trellocards.dagger.annotations.ForActivity;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */
@Module(injects = { StartActivity.class, MainActivity.class}, library = true,
        addsTo = ApplicationModule.class)
public class ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    @Singleton
    @ForActivity
    Context provideActivityContext() {
        return activity;
    }

    @Provides
    @Singleton
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @Provides @Singleton
    LayoutInflater provideLayoutInflater() {
        return LayoutInflater.from(activity);
    }
}

package trello.com.trellocards.api;

import android.content.Context;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import trello.com.trellocards.dagger.annotations.ForApplication;
import trello.com.trellocards.events.ProgressEvent;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */
public class ApiController {

    @Inject Bus bus;
    @Inject ApiClient api;
    @Inject @ForApplication
    Context context;

    private int numberOfTasks;

    public void onResume() {
        numberOfTasks = 0;
        bus.register(this);
    }

    public void onPause() {
        numberOfTasks = 0;
        bus.unregister(this);
    }

    private abstract class ApiCallback<T> implements Callback<T> {
        {
            taskCreated();
        }

        @Override
        final public void success(T t, Response response) {
            taskFinished();
            finished(t, response);
        }

        abstract public void finished(T t, Response response);

        public void failure(RetrofitError retrofitError) {
            taskFinished();
            bus.post(retrofitError);
        }
    }

    private void taskCreated() {
        numberOfTasks++;
        bus.post(new ProgressEvent(numberOfTasks));
    }

    private void taskFinished() {
        numberOfTasks--;
        bus.post(new ProgressEvent(numberOfTasks));
    }

    @Produce
    public ProgressEvent getProgress() {
        return new ProgressEvent(numberOfTasks);
    }


}

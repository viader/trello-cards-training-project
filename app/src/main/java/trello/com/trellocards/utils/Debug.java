package trello.com.trellocards.utils;

import android.util.Log;

import trello.com.trellocards.BuildConfig;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */
public class Debug {

    private static final String TAG = "TRELLO";

    public static final int NONE = 1;
    public static final int INFO = 2;
    public static final int DEBUG = 3;
    public static final int DEBUG_MORE = 4;

    /**
     * Verbose level of application log. Eg. DEBUG level will show NONE, INFO
     * and DEBUG messages, but not VERBOSE messages.
     */
    private static final int MODE = BuildConfig.DEBUG ? DEBUG_MORE : NONE;


    public static void debugMore(String str)
    {
        if (getMode() >= DEBUG_MORE)
            Log.v(TAG, str);
    }


    public static void debug(String str)
    {
        if (getMode() >= DEBUG)
            Log.d(TAG, str);
    }

    public static void info(String str)
    {
        if (getMode() >= INFO)
            Log.i(TAG, str);
    }

    public static void error(String string)
    {
        Log.e(TAG, string);
    }

    public static int getMode()
    {
        return MODE;
    }

}

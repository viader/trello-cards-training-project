package trello.com.trellocards;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentActivity;

import com.squareup.otto.Bus;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;
import trello.com.trellocards.dagger.modules.ActivityModule;


public class BaseActivity extends FragmentActivity {
    private ObjectGraph activityGraph;

    @Inject protected Bus bus;
    @Inject protected SharedPreferences preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TrelloApplication application = (TrelloApplication) getApplication();
        activityGraph = application.getApplicationGraph().plus(getModules().toArray());
        activityGraph.inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }


    @Override
    protected void onDestroy() {
        activityGraph = null;
        super.onDestroy();
    }

    public void inject(Object object) {
        activityGraph.inject(object);
    }

    protected List<Object> getModules() {
        return Arrays.<Object> asList(new ActivityModule(this));
    }

    public void saveToken(String token) {
        preferences.edit().putString("TOKEN", token).apply();

    }

    public String getToken() {
        return preferences.getString("TOKEN","");
    }

}

package trello.com.trellocards;

import android.app.Application;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;
import trello.com.trellocards.api.ApiController;
import trello.com.trellocards.dagger.modules.ApplicationModule;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */
public class TrelloApplication extends Application {

    @Inject protected ApiController apiController;
    private ObjectGraph applicationGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationGraph = ObjectGraph.create(getModules().toArray());
        applicationGraph.inject(this);
        apiController.onResume();
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new ApplicationModule(this));
    }

    public ObjectGraph getApplicationGraph() {
        return applicationGraph;
    }
}

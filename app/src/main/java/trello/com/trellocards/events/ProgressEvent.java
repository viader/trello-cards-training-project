package trello.com.trellocards.events;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 04.06.15.
 */

public class ProgressEvent {
    private final int numberOfTasks;

    public ProgressEvent(int numberOfTasks) {
        this.numberOfTasks = numberOfTasks;
    }

    public int getNumberOfTasks() {
        return numberOfTasks;
    }
}

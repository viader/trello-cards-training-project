package trello.com.trellocards;

/**
 * Created by Maciej Kalinowski (koliczyna@gmail.com) on 05.06.15.
 */
public class TrelloConstants {

    public static final String APP_NAME = "Maciej Kalinowski app";

    public static final String KEY_QUERY = "key=";
    public static final String KEY_NAME = "name=";

    public static final String TOKEN_REQUEST_URL = "https://trello.com/1/authorize?" + KEY_QUERY + BuildConfig.KEY
            + "&" + KEY_NAME + APP_NAME + "&expiration=30days&response_type=token&scope=read,write";
}
